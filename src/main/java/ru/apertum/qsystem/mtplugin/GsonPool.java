/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.apertum.qsystem.mtplugin;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import java.awt.Color;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.util.Date;
import ru.apertum.qsystem.client.Locales;
import ru.apertum.qsystem.common.Uses;

/**
 *
 * @author Evgeniy Egorov
 */
public class GsonPool {

    final GsonBuilder gsonb;

    private GsonPool() {
        gsonb = new GsonBuilder();
        final GsonPool.DateSerializer ds = new GsonPool.DateSerializer();
        final GsonPool.ColorSerializer cs = new GsonPool.ColorSerializer();
        gsonb.registerTypeHierarchyAdapter(Date.class, ds);
        gsonb.registerTypeHierarchyAdapter(Color.class, cs);
    }

    public static GsonPool get() {
        return GsonPoolHolder.INSTANCE;
    }

    private static class GsonPoolHolder {

        private static final GsonPool INSTANCE = new GsonPool();
    }

    public Gson it() {
        return gsonb.excludeFieldsWithoutExposeAnnotation().create();
    }

    private static class ColorSerializer implements JsonDeserializer<Color>, JsonSerializer<Color> {

        @Override
        public JsonElement serialize(Color arg0, Type arg1, JsonSerializationContext arg2) {
            return new JsonPrimitive(arg0.getRGB());
        }

        @Override
        public Color deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            return new Color(json.getAsInt());
        }
    }

    private static class DateSerializer implements JsonDeserializer<Date>, JsonSerializer<Date> {

        @Override
        public JsonElement serialize(Date arg0, Type arg1, JsonSerializationContext arg2) {
            return new JsonPrimitive(Locales.getInstance().formatDdMmYyyyTime.format(arg0));
        }

        @Override
        public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            try {
                return Locales.getInstance().formatDdMmYyyyTime.parse(json.getAsString());
            } catch (ParseException ex) {
                throw new RuntimeException("Not pars JSON by proxy!", ex);
            }
        }
    }
}
