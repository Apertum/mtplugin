/*
 * Copyright (C) 2014 Evgeniy Egorov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ru.apertum.qsystem.mtplugin;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 *
 * @author Evgeniy Egorov
 */
public class About {
    
    public final static String DATE = "date";
    public final static String VERSION = "version";
    public final static String QSYSTEM = "qsystem";
    public final static String UID = "UID";

    public static String ver = "";
    public static String date = "";
    public static String uid = "";
    public static String qsystem = "";

    public static void load() {
        final Properties settings = new Properties();
        final InputStream inStream = settings.getClass().getResourceAsStream("/ru/apertum/qsystem/mtplugin/mtplugin.properties");

        try {
            settings.load(inStream);
        } catch (IOException ex) {
            throw new RuntimeException("Cant read version. " + ex);
        }
        ver = settings.getProperty(VERSION);
        date = settings.getProperty(DATE);
        uid = settings.getProperty(UID);
        qsystem = settings.getProperty(QSYSTEM);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        final Properties settings = new Properties();
        final InputStream inStream = settings.getClass().getResourceAsStream("/ru/apertum/qsystem/mtplugin/mtplugin.properties");

        try {
            settings.load(inStream);
        } catch (IOException ex) {
            throw new RuntimeException("Cant read version. " + ex);
        }
        load();
        System.out.println("**** MTplugin ****");
        System.out.println("  version " + ver);
        System.out.println("  QSystem version " + qsystem);
        System.out.println("  date " + date);
        System.out.println("  UID " + uid);
        System.out.println("******************");
        
        
    }

}
